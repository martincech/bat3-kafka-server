#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

CONFIG_FILE="install.properties"

KAFKA_PATH="/usr/kafka"
CONFIG_PATH="${KAFKA_PATH}/config"
MANAGER="kafka-manager"

BROKER_HEAP_SIZE="1024"
ZOOKEEPER_HEAP_SIZE="1024"
REST_HEAP_SIZE="256"
SCHEMA_HEAP_SIZE="256"
MANAGER_HEAP_SIZE="512"

BROKER_PORT="9092"
ZOOKEEPER_PORT="2181"
MANAGER_PORT="8080"
SCHEMA_PORT="8081"
REST_PORT="8082"
JMX_PORT="8083"

LOCAL_ADDRESS="127.0.0.1"
REMOTE_ADDRESS=""

KAFKA_TAR_NAME="confluent-2.0.1-2.11.7.tar.gz"
KAFKA_URL="http://packages.confluent.io/archive/2.0/"$KAFKA_TAR_NAME

KAFKA_MANAGER_URL="$MANAGER.tar"
CONFIG_FILES=(zookeeper.properties broker.properties schema.properties rest.properties application.conf log4j.properties connect-log4j.properties test-log4j.properties tools-log4j.properties)

ZOOKEEPER_SERVICE="zookeeper.service"
BROKER_SERVICE="broker.service"
REST_SERVICE="rest.service"
SCHEMA_SERVICE="schema.service"
MANAGER_SERVICE="manager.service"
SERVICES=($ZOOKEEPER_SERVICE $BROKER_SERVICE $REST_SERVICE $SCHEMA_SERVICE $MANAGER_SERVICE)
PREDEFINED_TOPICS="predefinedTopics.csv"

CheckJavaVersion()
{
   if type -p java; then
    JAVA=java
   elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
       JAVA="$JAVA_HOME/bin/java"
   else
       echo "no java"
   fi

   if [[ "$JAVA" ]]; then
       version=$("$JAVA" -version 2>&1 | awk -F '"' '/version/ {print $2}')
       if [[ "$version" != "1.8" ]]; then
          GetAndInstallJava8
          UpdateJavaAlternatives
       fi
   else 
      GetAndInstallJava8
      ln -s "$KAFKA_PATH/jdk1.8.0_92/bin/java" "/usr/bin"
   fi
}

GetAndInstallJava8()
{
   MACHINE_TYPE=`uname -m`
   if [ ${MACHINE_TYPE} == 'x86_64' ]; then
     # 64-bit stuff here
     JAVA_TAR_NAME="server-jre-8u92-linux-x64.tar.gz"
   else
     # 32-bit stuff here
     JAVA_TAR_NAME="jre-8u92-linux-i586.tar.gz"
   fi
   if [ -f ${JAVA_TAR_NAME} ]; then
      sudo cp ./${JAVA_TAR_NAME} ${KAFKA_PATH}/
   fi
   if [ ! -e ${KAFKA_PATH}/${JAVA_TAR_NAME} ]; then
      wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u92-b14/"$JAVA_TAR_NAME -P ${KAFKA_PATH}
   fi   
   tar -zxvf ${KAFKA_PATH}/${JAVA_TAR_NAME} -C ${KAFKA_PATH}
}

UpdateJavaAlternatives()
{
   update-alternatives --install /usr/bin/java java /opt/java/jdk1.8.0_92/bin/java 100  
   update-alternatives --config java
}

DownloadAndExtract() {
   if [ ! -d ${KAFKA_PATH} ] ; then      
      echo "--> Creating kafka folder."
      mkdir ${KAFKA_PATH}     
   fi

   CheckJavaVersion
   echo "--> Kafka broker path : '${KAFKA_PATH}'"   
   if [ -f ${KAFKA_TAR_NAME} ]; then
      sudo cp ./${KAFKA_TAR_NAME} ${KAFKA_PATH}/
   fi
   
   if [ ! -e ${KAFKA_PATH}/${KAFKA_TAR_NAME} ] ; then
      wget ${KAFKA_URL} -P ${KAFKA_PATH}    
   fi
 
   echo "--> Extracting Kafka broker."
   tar xzf ${KAFKA_PATH}/${KAFKA_TAR_NAME} -C ${KAFKA_PATH} --strip-components=1

   echo "--> Extracting Kafka manager."
   tar xf ${KAFKA_MANAGER_URL} -C ${KAFKA_PATH}
   
   #cp all configuration
   CopyConfiguration
   CopyServices
}

CopyConfiguration()
{
   echo "--> Copying configuration."
   if [ ! -d ${KAFKA_PATH}/config ] ; then
      mkdir ${KAFKA_PATH}/config
   else
      rm -f ${KAFKA_PATH}/config/*
   fi
   #replace all the variables in configuration files
   for prop in ${CONFIG_FILES[@]}; 
   do
     sudo cp ./$prop ${KAFKA_PATH}/config/
     ReplaceVariables ${KAFKA_PATH}/config/$prop
   done
}

ReplaceVariables()
{
   sed -i -e "s,@KAFKA_PATH@,"${KAFKA_PATH}",g" -e "s,@MANAGER@,"${MANAGER}",g" -e "s,@CONFIG_PATH@,"${CONFIG_PATH}",g" $1
   sed -i -e "s,@BROKER_PORT@,"${BROKER_PORT}",g" -e "s,@BROKER_HEAP_SIZE@,"${BROKER_HEAP_SIZE}",g" $1
   sed -i -e "s,@ZOOKEEPER_PORT@,"${ZOOKEEPER_PORT}",g" -e "s,@ZOOKEEPER_HEAP_SIZE@,"${ZOOKEEPER_HEAP_SIZE}",g" $1
   sed -i -e "s,@SCHEMA_HEAP_SIZE@,"${SCHEMA_HEAP_SIZE}",g" -e "s,@REST_HEAP_SIZE@,"${REST_HEAP_SIZE}",g" -e "s,@MANAGER_HEAP_SIZE@,"${MANAGER_HEAP_SIZE}",g" $1
   sed -i -e "s,@SCHEMA_PORT@,"${SCHEMA_PORT}",g" -e "s,@REST_PORT@,"${REST_PORT}",g" $1
   sed -i -e "s,@MANAGER_PORT@,"${MANAGER_PORT}",g" -e "s,@JMX_PORT@,"${JMX_PORT}",g" $1
   sed -i -e "s,@LOCAL_ADDRESS@,"${LOCAL_ADDRESS}",g" -e "s,@REMOTE_ADDRESS@,"${REMOTE_ADDRESS}",g" $1
}

UninstallAndDelete()
{
   rm -rf ${KAFKA_PATH}
   rm -f /usr/bin/java
}

CopyServices()
{
   echo "--> Copying services."
   if [ ! -d ${KAFKA_PATH}/services ] ; then
      mkdir ${KAFKA_PATH}/services
   else
      rm -f ${KAFKA_PATH}/services/*
   fi

   for prop in ${SERVICES[@]}; 
   do
     sudo cp ./$prop ${KAFKA_PATH}/services/
     ReplaceVariables ${KAFKA_PATH}/services/$prop
   done
}

RegisterServices()
{  
   for prop in ${SERVICES[@]}; 
   do
     RegisterService $prop
   done
}

RegisterService()
{
   systemctl enable ${KAFKA_PATH}/services/$1
}

UnRegisterServices()
{
   for prop in ${SERVICES[@]}; 
   do
     UnRegisterService $prop
   done
}

UnRegisterService()
{
   systemctl disable $1
}

StartServices()
{
   for prop in ${SERVICES[@]}; 
   do
     StartService $prop
   done
}

StartService()
{
   systemctl start $1
}

StopServices()
{
   for prop in ${SERVICES[@]}; 
   do
     StopService $prop
   done
}

StopService()
{
   systemctl stop $1
}

StatusServices()
{
   for prop in ${SERVICES[@]}; 
   do
     StatusService $prop
   done
}

StatusService()
{
   systemctl status $1
}

ReadConfigurationFromFile()
{
   IFS="="
   while read -r name value
   do
   case "$name" in
   "local.address")
         LOCAL_ADDRESS=$value
         ;;
   "remote.address")
         REMOTE_ADDRESS=$value
         ;;
   "broker.heap.size")
         BROKER_HEAP_SIZE=$value
         ;;
   "zookeeper.heap.size")
         ZOOKEEPER_HEAP_SIZE=$value
         ;;
   "rest.heap.size")
         REST_HEAP_SIZE=$value
         ;;
   "schema.heap.size")
         SCHEMA_HEAP_SIZE=$value
         ;;
   "manager.heap.size")
         MANAGER_HEAP_SIZE=$value
         ;;
   "broker.port")
         BROKER_PORT=$value
         ;;
   "zookeeper.port")
         ZOOKEEPER_PORT=$value
         ;;
   "manager.port")
         MANAGER_PORT=$value
         ;;
   "schema.port")
         SCHEMA_PORT=$value
         ;;
   "rest.port")
         REST_PORT=$value
         ;;
   "jmx.port")
         JMX_PORT=$value
         ;;
   esac
   done < $1
}

CheckRequiredParameters()
{
   if [ "$REMOTE_ADDRESS" == "" ]
   then
      echo "Enter remote address (Domain name is preferred)"
      read REMOTE_ADDRESS
   fi
}

LoadSettings()
{
   if [ -f "$CONFIG_FILE" ]
   then
      echo "== Read settings from file =="
      ReadConfigurationFromFile $CONFIG_FILE
   fi
   CheckRequiredParameters
}

CreateTopics()
{
   existingTopics=()
   list=$(${KAFKA_PATH}/bin/kafka-topics --list --zookeeper ${REMOTE_ADDRESS}:${ZOOKEEPER_PORT})
   readarray -t existingTopics <<< "$list"
   
   echo "== Create predefined topics =="
   sed -n '2,$p' < $PREDEFINED_TOPICS |   #first row contains header
   while IFS=";" read -r name partitions replicas
   do
     if [[ "${existingTopics[@]}" =~ "${name}" ]]; then
         echo "Topic: " ${name} " already exist"
      else
         ${KAFKA_PATH}/bin/kafka-topics --create --zookeeper ${REMOTE_ADDRESS}:${ZOOKEEPER_PORT} --replication-factor ${replicas} --partitions ${partitions} --topic ${name}
      fi
   done
}

ShowHelp() 
{
   echo " -h      Show this help"
   echo " -i      Download and install all the files and services and start them"
   echo " start   Start the services"
   echo " stop    Stop the services"
   echo " status  Status the services"
   echo " -u    Uninstall all the services and delete files"
}

case "$1" in
   -u)   echo "!! ->    Uninstalling ...."  
         StopServices
         sleep 1 
         UnRegisterServices
         UninstallAndDelete
         ;;
   -i)   echo  "!! ->  Installing ...."
         LoadSettings
         DownloadAndExtract
         RegisterServices
         ;;
   register)
         echo  "!! ->  Registering services ...."
         RegisterServices
         ;;
   deregister)
         echo  "!! ->  Unregistering services ...."
         UnRegisterServices
         ;;
   start)
         echo  "!! ->  Starting services ...."         
         LoadSettings
         CopyConfiguration
         CopyServices         
         StartServices
         CreateTopics         
         ;;
   stop) echo  "!! ->  Stopping services ...."
         StopServices         
         ;;
   status)
         StatusServices         
         ;;
   log)
         for prop in ${SERVICES[@]}; 
         do
            journalctl -u $prop > /media/sf_shared/$prop.log
         done
         
         ;;
   *)    ShowHelp
         ;;
esac